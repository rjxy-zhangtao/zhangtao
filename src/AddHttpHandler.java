import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

public class AddHttpHandler extends Common implements HttpHandler {
    @Override
    public void handle(HttpExchange httpExchange) {
        try {
            StringBuilder responseText = new StringBuilder();
            int a = Integer.valueOf(getRequestParam(httpExchange, "a"));
            int b = Integer.valueOf(getRequestParam(httpExchange, "b"));
            responseText.append("结果：").append(sum(a, b)).append("<br/>");
            handleResponse(httpExchange, responseText.toString());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private String sum(int a, int b) {
        return String.valueOf(a+b);
    }

}



