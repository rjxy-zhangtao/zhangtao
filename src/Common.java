import com.sun.net.httpserver.HttpExchange;
import java.io.OutputStream;
import java.net.URLDecoder;
import java.util.HashMap;

public class Common {

    public String getRequestParam(HttpExchange httpExchange, String string) throws Exception {
        String paramStr = "";
        paramStr = httpExchange.getRequestURI().getQuery();
        String[] segment = paramStr.split("&");
        HashMap<String ,String> map=new HashMap<>();
        for (String kvString : segment) {
            String[] kv = kvString.split("=");
            String key = URLDecoder.decode(kv[0], "UTF-8");
            String value = "";
            if (kv.length == 2) {
                value = URLDecoder.decode(kv[1], "UTF-8");
            }
            map.put(key, value);
        }
        return map.get(string);
    }

    public void handleResponse(HttpExchange httpExchange, String responsetext) throws Exception {
        StringBuilder responseContent = new StringBuilder();
        responseContent.append("<html>")
                .append("<body>")
                .append(responsetext)
                .append("</body>")
                .append("</html>");
        String responseContentStr = responseContent.toString();
        byte[] responseContentByte = responseContentStr.getBytes("utf-8");
        httpExchange.getResponseHeaders().add("Content-Type:", "text/html;charset=utf-8");
        httpExchange.sendResponseHeaders(200, responseContentByte.length);
        OutputStream out = httpExchange.getResponseBody();
        out.write(responseContentByte);
        out.flush();
        out.close();
    }
}
