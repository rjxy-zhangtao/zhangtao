import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

public class MultHttpHandler extends Common implements HttpHandler {
    @Override
    public void handle(HttpExchange httpExchange) {
        try {
            StringBuilder responseText = new StringBuilder();
            int a = Integer.valueOf(getRequestParam(httpExchange, "a"));
            int b = Integer.valueOf(getRequestParam(httpExchange, "b"));
            responseText.append("结果：").append(mult(a, b)).append("<br/>");
            handleResponse(httpExchange, responseText.toString());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private String mult(int a, int b) {
        return String.valueOf(a*b);
    }

}



